<section class="facility pb_l mb_l">
    <div class="wrapper">
        <h2><img class="pc mb mt headline" src="<?php bloginfo('template_url'); ?>/images/head_anshinanzen.svg" alt="安心・安全"></h2>
        <h2><img class="head sp pt_l pb_l" src="<?php bloginfo('template_url'); ?>/images/head_anshinanzen_sp.svg" alt="安心・安全"></h2>
        <p class="center mb_l">体調管理・安全対策の最新ツールを導入。<br class="pc">さらに、快適な環境と万全のセキュリティを完備。</p>

        <div class="outer cf">
            <img class="pc star_left" src="<?php bloginfo('template_url'); ?>/images/facility_star_l1.png" />
            <img class="pc star_right" src="<?php bloginfo('template_url'); ?>/images/facility_star_r1.png" />
            <div class="left">
                <div class="photo">
                    <img class="sp sp_right" src="<?php bloginfo('template_url'); ?>/images/facility_star_r2.png" />
                    <img class="sp sp_left" src="<?php bloginfo('template_url'); ?>/images/facility_star_r2.png" />

                    <img class="main" src="<?php bloginfo('template_url'); ?>/images/facility_anshin1_photo.jpg?v=20190403" />
                    <img class="pc star" src="<?php bloginfo('template_url'); ?>/images/facility_star_c.png" />
                    <img class="icon" src="<?php bloginfo('template_url'); ?>/images/facility_anshin1_icon.svg" />
                </div>
                <!-- photo -->
                <div class="text">
                    <h3><img src="<?php bloginfo('template_url'); ?>/images/facility_anshin1_title.svg" alt="シャワー室の完備" /></h3>
                    <p>感染症や、夏季に汗をかく事で急な体温の変化で起きる体調不良を予防する為、幼児用シャワーを完備します。お子様に清潔で快適な環境をご用意します。</p>
                </div>
                <!-- text -->
            </div>
            <!-- left -->
            <div class="right">
                <div class="photo">
                    <img class="sp sp_right" src="<?php bloginfo('template_url'); ?>/images/facility_star_r2.png" />
                    <img class="sp sp_left" src="<?php bloginfo('template_url'); ?>/images/facility_star_r2.png" />

                    <img class="main" src="<?php bloginfo('template_url'); ?>/images/facility_anshin2_photo.jpg?v=20190403" />
                    <img class="pc star" src="<?php bloginfo('template_url'); ?>/images/facility_star_r2.png" />
                    <img class="icon" src="<?php bloginfo('template_url'); ?>/images/facility_anshin2_icon.svg" />
                    <img class="child" src="<?php bloginfo('template_url'); ?>/images/facility_anshin2_child.svg" alt="避難訓練の実施も行なっています。" />
                </div>
                <!-- photo -->
                <div class="text">
                    <h3><img src="<?php bloginfo('template_url'); ?>/images/facility_anshin2_title.svg" alt="防犯機器完備" /></h3>
                    <p>セキュリティー機器の完備、月1回、避難訓練を徹底します。</p>
                </div>
                <!-- text -->
            </div>
            <!-- right -->
        </div>
        <!-- outer -->
    </div>
    <!-- wrapper -->

</section>
<!-- facility -->

<section class="shokai pt bg_thema1">
    <div class="wrapper">
        <h2><img class="pc mb_l mt_l headline" src="<?php bloginfo('template_url'); ?>/images/head_facility_shokai.svg" alt="各施設のご紹介"></h2>
        <h2><img class="head sp pt_l pb_l" src="<?php bloginfo('template_url'); ?>/images/head_facility_shokai_sp.svg" alt="各施設のご紹介"></h2>
        <ul class="linkbtn cf pt_l pb_l">
            <li class="linkbtn2"><a href="#01">白石園</a></li>
            <li class="linkbtn2"><a href="#02">麻生園</a></li>
            <li class="linkbtn2"><a href="#03">月寒中央園</a></li>
            <li class="linkbtn2"><a href="#04">東区役所前園</a></li>
        </ul>
        <div class="facility pb_s" id="01">
            <h3><img class="" src="<?php bloginfo('template_url'); ?>/images/facility_icon.svg"><span class="text">白石園</span><span class="num">定員19名</span></h3>
            <div class="outer cf">
                <div class="left">
                    <table class="style02" cellspacing="0" cellpadding="0">
                        <tr>
                            <th>住所</th>
                            <td>札幌市白石区南郷通1丁目8番地ディノス白石</td>
                        </tr>
                        <tr>
                            <th>営業時間</th>
                            <td>7:30～20:30<p class="small">※18:30以降のお預かりは事前相談が必要になります。</p>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- left -->
                <div class="right">
                    <table class="style02" cellspacing="0" cellpadding="0">
                        <tr>
                            <th>開園日</th>
                            <td>月～日・祝</td>
                        </tr>
                        <tr>
                            <th>最寄り駅</th>
                            <td>地下鉄東西線白石駅徒歩2分</td>
                        </tr>
                        <tr>
                            <th>駐車場</th>
                            <td>有<br>※お子様を預けている間は無料で駐車場を利用できます</td>
                        </tr>
                    </table>
                    <p class="small mt_s pt_s">※土日祝に保護者自由参加型のイベントを開く場合がございます。</p>
                    <p class="small">※土日祝日は8:00～17:00開園</p>
                    <p class="small">※休園日はその他、年末年始となります</p>
                </div>
                <!-- right -->
            </div>
            <!-- outer -->
            <ul class="gallery cf">
                <li><img class="main" src="<?php bloginfo('template_url'); ?>/images/facility_shiroishi_photo1.jpg" /></li>
                <li><img class="main" src="<?php bloginfo('template_url'); ?>/images/facility_shiroishi_photo2.jpg" /></li>
                <li><img class="main" src="<?php bloginfo('template_url'); ?>/images/facility_shiroishi_photo3.jpg" /></li>
                <li><img class="main" src="<?php bloginfo('template_url'); ?>/images/facility_shiroishi_photo4.jpg" /></li>
            </ul>
            <iframe class="pt mt_s pb" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d612.9554448212243!2d141.3980862094673!3d43.04723774773798!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f33.1!3m3!1m2!1s0x0%3A0x3acb43dc00d7b72a!2z44OH44Kj44OO44K5IOacreW5jOeZveefsw!5e0!3m2!1sja!2sjp!4v1551161454716" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <!-- facility -->

        <div class="facility pb_s" id="02">
            <h3><img class="" src="<?php bloginfo('template_url'); ?>/images/facility_icon.svg"><span class="text">麻生園</span><span class="num">定員19名</span></h3>
            <div class="outer cf">
                <div class="left">
                    <table class="style02" cellspacing="0" cellpadding="0">
                        <tr>
                            <th>住所</th>
                            <td>札幌市北区北40条西4丁目1番1号ASABU LAND</td>
                        </tr>
                        <tr>
                            <th>営業時間</th>
                            <td>7:30～20:30<p class="small">※18:30以降のお預かりは事前相談が必要になります。</p>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- left -->
                <div class="right">
                    <table class="style02" cellspacing="0" cellpadding="0">
                        <tr>
                            <th>開園日</th>
                            <td>月～日・祝</td>
                        </tr>
                        <tr>
                            <th>最寄り駅</th>
                            <td>地下鉄南北線麻生駅徒歩1分</td>
                        </tr>
                    </table>
                    <p class="small mt_s pt_s">※土日祝に保護者自由参加型のイベントを開く場合がございます。</p>
                    <p class="small">※土日祝日は8:00～17:00開園</p>
                    <p class="small">※休園日はその他、年末年始となります</p>
                </div>
                <!-- right -->
            </div>
            <!-- outer -->
            <ul class="gallery cf">
                <li><img class="main" src="<?php bloginfo('template_url'); ?>/images/facility_azabu_photo1.jpg" /></li>
                <li><img class="main" src="<?php bloginfo('template_url'); ?>/images/facility_azabu_photo2.jpg" /></li>
                <li><img class="main" src="<?php bloginfo('template_url'); ?>/images/facility_azabu_photo3.jpg" /></li>
                <li><img class="main" src="<?php bloginfo('template_url'); ?>/images/facility_azabu_photo4.jpg" /></li>
            </ul>

            <iframe class="pt mt_s pb" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1588.2276401101165!2d141.3384750155524!3d43.10824973338701!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbb8600f7e48c93f3!2sASABU+LAND!5e0!3m2!1sja!2sjp!4v1551161798820" frameborder="0" style="border:0" allowfullscreen></iframe>

        </div>
        <!-- facility -->

        <div class="facility pb_s" id="03">
            <h3><img class="" src="<?php bloginfo('template_url'); ?>/images/facility_icon.svg"><span class="text">月寒中央園</span><span class="num">定員12名</span></h3>
            <div class="outer cf">
                <div class="left">
                    <table class="style02" cellspacing="0" cellpadding="0">
                        <tr>
                            <th>住所</th>
                            <td>札幌市豊平区月寒中央通9丁目3-37CBSビル1F</td>
                        </tr>
                        <tr>
                            <th>営業時間</th>
                            <td>7:30～20:30<p class="small">※18:30以降のお預かりは事前相談が必要になります。</p>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- left -->
                <div class="right">
                    <table class="style02" cellspacing="0" cellpadding="0">
                        <tr>
                            <th>開園日</th>
                            <td>月～日・祝</td>
                        </tr>
                        <tr>
                            <th>最寄り駅</th>
                            <td>地下鉄東豊線月寒中央駅徒歩4分</td>
                        </tr>
                    </table>
                    <p class="small mt_s pt_s">※土日祝に保護者自由参加型のイベントを開く場合がございます。</p>
                    <p class="small">※土日祝日は8:00～17:00開園</p>
                    <p class="small">※休園日はその他、年末年始となります</p>
                </div>
                <!-- right -->
            </div>
            <!-- outer -->
            <ul class="gallery cf">
                <li><img class="main" src="<?php bloginfo('template_url'); ?>/images/facility_tsukisamu_photo1.jpg" /></li>
                <li><img class="main" src="<?php bloginfo('template_url'); ?>/images/facility_tsukisamu_photo2.jpg" /></li>
                <li><img class="main" src="<?php bloginfo('template_url'); ?>/images/facility_tsukisamu_photo3.jpg" /></li>
                <li><img class="main" src="<?php bloginfo('template_url'); ?>/images/facility_tsukisamu_photo4.jpg" /></li>
            </ul>
            <iframe class="pt mt_s pb" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d182.29063361491094!2d141.39822482882548!3d43.027750725988525!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f123.1!3m3!1m2!1s0x5f0b2a565f9fd8f9%3A0x853a87cc5a02878e!2z44CSMDYyLTAwMjAg5YyX5rW36YGT5pyt5bmM5biC6LGK5bmz5Yy65pyI5a-S5Lit5aSu6YCa77yZ5LiB55uu77yTIO-8o--8ju-8ou-8ju-8s--8juODk-ODqw!5e0!3m2!1sja!2sjp!4v1551253897896" frameborder="0" style="border:0" allowfullscreen></iframe>

        </div>
        <!-- facility -->

        <div class="facility pb_s" id="04">
            <h3><img class="" src="<?php bloginfo('template_url'); ?>/images/facility_icon.svg"><span class="text">東区役所前園</span><span class="num">定員19名</span></h3>
            <div class="outer cf">
                <div class="left">
                    <table class="style02" cellspacing="0" cellpadding="0">
                        <tr>
                            <th>住所</th>
                            <td>札幌市東区北14条東8丁目3番1号BEビル1F</td>
                        </tr>
                        <tr>
                            <th>営業時間</th>
                            <td>7:30～20:30<p class="small">※18:30以降のお預かりは事前相談が必要になります。</p>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- left -->
                <div class="right">
                    <table class="style02" cellspacing="0" cellpadding="0">
                        <tr>
                            <th>開園日</th>
                            <td>月～日・祝</td>
                        </tr>
                        <tr>
                            <th>最寄り駅</th>
                            <td>地下鉄東豊線東区役所前徒歩4分</td>
                        </tr>
                    </table>
                    <p class="small mt_s pt_s">※土日祝に保護者自由参加型のイベントを開く場合がございます。</p>
                    <p class="small">※土日祝日は8:00～17:00開園</p>
                    <p class="small">※休園日はその他、年末年始となります</p>
                </div>
                <!-- right -->
            </div>
            <!-- outer -->
            <ul class="gallery cf">
                <li><img class="main" src="<?php bloginfo('template_url'); ?>/images/facility_higashi_photo1.jpg" /></li>
                <li><img class="main" src="<?php bloginfo('template_url'); ?>/images/facility_higashi_photo2.jpg" /></li>
                <li><img class="main" src="<?php bloginfo('template_url'); ?>/images/facility_higashi_photo3.jpg" /></li>
                <li><img class="main" src="<?php bloginfo('template_url'); ?>/images/facility_higashi_photo4.jpg" /></li>
            </ul>
            <iframe class="pt mt_s pb" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d578.2440830410271!2d141.36412382669207!3d43.07997569646331!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f63.1!3m3!1m2!1s0x5f0b291611d7ca7f%3A0xd22828d9fed73194!2z44CSMDY1LTAwMTQg5YyX5rW36YGT5pyt5bmM5biC5p2x5Yy65YyX77yR77yU5p2h5p2x77yY5LiB55uu77yT4oiS77yRIOODk-ODvOODk-ODqw!5e0!3m2!1sja!2sjp!4v1551163316810" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <!-- facility -->

    </div>
    <!-- wrapper -->
    <div class="footer_photo2 mt"></div>
</section>
<!-- shokai -->
