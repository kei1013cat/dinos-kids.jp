
<?php get_header(); ?>
<?php include (TEMPLATEPATH . '/part-title.php'); ?>

<div id="main_contents" class="cf">
    <div id="contents" class="mr15">
        <section class="maintenance">
            <div class="wrapper">
            <p class="tac pt_l">
            大変申し訳ございませんがこちらのページは<br>
            ただ今準備中となっております。<br>
            </p>
            <p class="pt linkbtn1 pb_l"><a href="<?php bloginfo('url'); ?>/">トップページへ戻る</a></p>
            </div>
        </section>
        <!-- maintenance -->
    </div>
    <!-- contents -->

</div>
<!-- main_contents --> 

<?php get_footer(); ?>
