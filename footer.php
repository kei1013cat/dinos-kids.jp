<!-- contents -->
<div class="footer_photo"></div>
<footer>
    <div class="wrapper outer cf">
        <div class="outer_left">

            <div class="sp inner cf">
                <nav class="cf">
                    <ul class="inner_left">
                        <li><a href="<?php bloginfo('url'); ?>/about/">当園について</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/facility/">施設紹介</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/program/">保育内容</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/price/">ご利用料金</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/question/">よくあるご質問</a></li>
                        <li class="contact">
                            <a href="<?php bloginfo('url'); ?>/contact/"><img src="<?php bloginfo('template_url'); ?>/images/header_contact_sp.png" alt="お問い合わせ" />
                            </a>
                        </li>
                    </ul>
                    <ul class="inner_right">
                        <li><a href="https://sdentertainment.jp/company/profile.php" target="_blank">会社概要</a></li>
                        <li><a href="https://sdentertainment.jp/privacy/" target="_blank">プライバシーポリシー</a></li>
                        <li class="recruit"><a href="<<?php bloginfo('url'); ?>/recruit/"><img src="<?php bloginfo('template_url'); ?>/images/footer_recruit_linkbtn.svg" alt="採用情報" /></a></li>
                    </ul>
                </nav>
            </div>
            <!-- inner -->

            <div class="comp">
                <h2 class="logo mt_l"><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/footer_logo.svg" alt="ディノスキッズ" class="logo" /></a></h2>
                <p class="pt_s pb_s">合同説明会・見学会を随時開催中！</p>
                <!--          <a href="tel:0359620031"><img class="tel" src="<?php bloginfo('template_url'); ?>/images/footer_tel.svg" alt="03-5962-0031" /></a>-->
            </div>
            <p class="copy sp">Copyright &copy; SDEntertainment,inc. All rights reserved.</p>
        </div>
        <!-- left -->
        <div class="outer_right pc">
            <div class="inner cf">
                <nav>
                    <ul class="inner_left">
                        <li><a href="<?php bloginfo('url'); ?>/about/">当園について</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/facility/">施設紹介</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/program/">保育内容</a></li>
                    </ul>
                    <ul class="inner_center">
                        <li><a href="<?php bloginfo('url'); ?>/price/">ご利用料金</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/question/">よくあるご質問</a></li>
                    </ul>
                    <ul class="inner_right">
                        <li><a href="https://sdentertainment.jp/company/profile.php" target="_blank">会社概要</a></li>
                        <li><a href="https://sdentertainment.jp/privacy/" target="_blank">プライバシーポリシー</a></li>
                    </ul>
                </nav>

                <div class="recruit">
                    <a class="contact" href="<?php bloginfo('url'); ?>/contact/"><img src="<?php bloginfo('template_url'); ?>/images/header_contact_sp.png" alt="お問い合わせ" />
                    </a> <a href="<?php bloginfo('url'); ?>/recruit/"><img src="<?php bloginfo('template_url'); ?>/images/footer_recruit_linkbtn.svg" alt="採用情報" /></a> </div>
                <!-- recruit -->
            </div>
            <!-- inner -->
            <p class="copy">Copyright &copy; SDEntertainment,inc. All rights reserved.</p>
        </div>
        <!-- right -->
    </div>

    <?php if(is_pc()):?>

    <p><a href="#" id="page-top"></a></p>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/top.js"></script>
    <?php endif; ?>
</footer>
</div>
<!-- outer -->


<script>
    <?php if(is_pc()):?>

    jQuery(function($) {
        var ua = navigator.userAgent;
        var $scrtgt = $(window); // スクロール対象：<html>
        if (ua.indexOf('MSIE') > 0 || ua.indexOf('Trident') > 0) {
            $('#page_index .photo1').css('background-attachment', 'scroll');
            $('#page_index .photo2').css('background-attachment', 'scroll');
        } else {
            $('#page_index .photo1').css('background-attachment', 'fixed');
            $('#page_index .photo2').css('background-attachment', 'fixed');
        }
        if (ua.indexOf('iPad') > 0 || ua.indexOf('Android') > 0) {
            $('#page_index .photo1').css('background-attachment', 'scroll');
            $('#page_index .photo2').css('background-attachment', 'scroll');
        } else {
            $('#page_index .photo1').css('background-attachment', 'fixed');
            $('#page_index .photo2').css('background-attachment', 'fixed');
        }
    });
    <?php endif; ?>


    $(function() {

        $("#openMenu").click(function() {
            $("#openMenu").hide();
            $("#closeMenu").show();
            $("#layerMenu").show();
            $("#overray").show();
        });
        $("#closeMenu").click(function() {
            $("#closeMenu").hide();
            $("#openMenu").show();
            $("#layerMenu").hide();
            $("#overray").hide();
        });
        $("#layerMenu a").click(function() {
            $("#closeMenu").hide();
            $("#openMenu").show();
            $("#layerMenu").hide();
            $("#overray").hide();
        });
        $("#overray").click(function() {
            $("#closeMenu").hide();
            $("#openMenu").show();
            $("#layerMenu").hide();
            $("#overray").hide();
        });
    });

</script>
<?php wp_footer(); ?>
</div>
<!-- outer -->

</body>

</html>
