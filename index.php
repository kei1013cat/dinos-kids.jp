<?php get_header(); ?>



<div id="contents">
  <section class="mainimage">
    <h2 class="enter-bottom"><img src="<?php bloginfo('template_url'); ?>/images/mainimage_text.svg" alt="笑顔あふれる保育園を目指して。"></h2>
    <p class="pc linkbtn enter-right"><a href="<?php bloginfo('url'); ?>/facility/"><img src="<?php bloginfo('template_url'); ?>/images/recruit_linkbtn.svg" alt="入園児募集中！詳細はこちら"></a></p>
    <p class="sp linkbtn"><a href="<?php bloginfo('url'); ?>/facility/"><img src="<?php bloginfo('template_url'); ?>/images/recruit_linkbtn_sp.svg" alt="入園児募集中！詳細はこちら"></a></p>
  </section>
    
  <section class="about pt_l pb_l mt mb">
      <img class="pc left" src="<?php bloginfo('template_url'); ?>/images/about_left.png"  />
      <img class="pc right" src="<?php bloginfo('template_url'); ?>/images/about_right.png" />
      <div class="wrapper">
      <p class="pt pb_s">こどもたちに、まるで遊園地に来たような<br>
        楽しいプログラムを設けることで、<br>
        ご自宅でも保育所でも毎日が<br class="sp">楽しくワクワクする<br>
        生活を実現したいと考えています。<br>
        毎日を全力で遊んで、心の「健康」と、<br>
        明日への「笑顔」に繋がっていただきたい<span class="dot">・・・</span><br>
        それがわたしたちの想いです。
      </p>
      <p class="pt linkbtn1 pb"><a href="<?php bloginfo('url'); ?>/about/">当園について</a></p>
      </div>
      <img class="bgimg sp" src="<?php bloginfo('template_url'); ?>/images/about_bg_sp.svg" />
  </section>
  <!-- about -->
  
  <section class="feature pt_l pb_l bg_thema1">
      <img class="img_left" src="<?php bloginfo('template_url'); ?>/images/feature_left.svg"  />
      <img class="img_right" src="<?php bloginfo('template_url'); ?>/images/feature_right.svg" />
      <div class="wrapper">
          <img class="pc pt_s pb_s" src="<?php bloginfo('template_url'); ?>/images/head_feature.svg" alt="ディノスキッズの特徴。">
          <img class="head sp pt_s pb_s" src="<?php bloginfo('template_url'); ?>/images/head_feature_sp.svg" alt="ディノスキッズの特徴。">
          <p class="pt_l">当園では、お子様を健やかに育めるよう、体調管理や安全対策には最新ツールを導入。<br>働くお父さまお母さまを応援するサービスもご用意しています。</p>
          <div class="outer pt cf">
              <div class="left">
                  <img src="<?php bloginfo('template_url'); ?>/images/feature_cont1.svg" alt="体調管理・安全対策の最新ツールを導入。">
              </div>
              <!-- left -->
              <div class="right">
                  <img src="<?php bloginfo('template_url'); ?>/images/feature_cont2.svg" alt="ベビーカー無料お預かりサービス。">
              </div>
              <!-- right -->
          </div>
          <!-- outer -->
          <p class="linkbtn1 pt_l pb"><a href="<?php bloginfo('url'); ?>/facility/">施設紹介</a></p>
      </div>
  </section>
  <!-- feature -->
    
  <section class="hoiku pt_l">
      <div class="wrapper">
          <img class="pc pt_s pb_s" src="<?php bloginfo('template_url'); ?>/images/head_hoiku.svg" alt="ディノスキッズの保育">
          <img class="head sp pt_s pb_s" src="<?php bloginfo('template_url'); ?>/images/head_hoiku_sp.svg" alt="ディノスキッズの保育">
      </div>
      <div class="photo"></div>
      <img class="sp" src="<?php bloginfo('template_url'); ?>/images/hoiku_bg_sp.svg" >
      <p>当園では、お子様ひとり一人の成長に合わせたカリキュラムを設けています。<br>
        また、こどもたちが幅広い遊びを通し、様々な人たちと関わりを持つことで、<br class="pc">
        こどもたちの社会性の発達や、地域社会の学び、<br class="pc">
        そして、子どもたちが切り開く未来の社会につなげていきます。</p>
      <p class="linkbtn1 pt_l pb"><a href="<?php bloginfo('url'); ?>/program/">保育内容</a></p>
  </section>
  
  <!-- お知らせ -->
  <?php
    $wp_query = new WP_Query();
    $param = array(
      'posts_per_page' => '2', //表示件数。-1なら全件表示
      'post_status' => 'publish',
      'orderby' => 'date', //ID順に並び替え
      'order' => 'DESC'
    );
    $wp_query->query($param);?>
  <?php if($wp_query->have_posts()):?>
  <section class="news mt_l pt_l mb_l">
    <div class="wrapper">
    <div class="outer cf">
        <div class="left">
            <img src="<?php bloginfo('template_url'); ?>/images/contact_icon.svg">
        </div>
        <!-- left -->
        <div class="right">
          <?php
            $i = 1;
            while($wp_query->have_posts()) :?>
          <?php $wp_query->the_post(); ?>
          <a href="#" class="open-options<?php echo $i; ?>">
          <dl class="cf">
            <dt>
              <?php the_time('Y.m.d'); ?>
            </dt>
            <dd><?php echo $post->post_title;?></dd>
          </dl>
          </a>
          <div class="modal-options<?php echo $i; ?>" data-izimodal-loop="" >
              <a class="modal-close" id="modal-close<?php echo $i; ?>" href="javascript:void(0)"><img src="<?php bloginfo('template_url'); ?>/images/modal_close.svg"></a>
              <div class="modal_title">
                  <p><span class="date"><?php the_time('Y.m.d'); ?></span>
                      <?php echo $post->post_title;?>
                  </p>
              </div>
              <div class="modal_content">
                  <?php the_content(); ?>
              </div>
	      </div>
            
          <?php 
            $i++;
            endwhile; ?>
          <?php endif; ?>
          <?php wp_reset_query(); ?>
        </div>
        <!-- right -->
        </div>
        <!-- outer -->
    </div>
    <!-- wrapper --> 
  </section>

</div>
<!--contents -->
<?php get_footer(); ?>
