<section class="question pb_l">
    <img class="pc left" src="<?php bloginfo('template_url'); ?>/images/question_left.png" />
    <img class="pc right" src="<?php bloginfo('template_url'); ?>/images/question_right.png" />
    <div class="wrapper">
        <h2><img class="pc mb_l mt_l headline" src="<?php bloginfo('template_url'); ?>/images/head_qa.svg" alt="Q&A"></h2>
        <h2><img class="head sp pt_l pb_l" src="<?php bloginfo('template_url'); ?>/images/head_qa_sp.svg" alt="Q&A"></h2>
        <p>大切なお子様をお預けいただく前に、知っておきたい、解決したい情報をまとめました。</p>

        <dl>
            <dt>見学は可能ですか？</dt>
            <dd>はい、可能です。自前にご予約下さい。お子様にお気に召す施設なのか是非ご覧ください。</dd>
        </dl>
        <dl>
            <dt>病気の時でも預かってもらえますか？</dt>
            <dd>病気のお子様、また熱が37.5℃以上あるお子様のお預りはできません。また感染症などの場合には医師の登園許可証が必要となります。</dd>
        </dl>
        <dl>
            <dt>送迎者に制限はありますか？</dt>
            <dd>登録頂いた御父兄のみとなります。防犯対策の為でございます。</dd>
        </dl>
        <dl>
            <dt>年齢別のクラス編成なのですか？</dt>
            <dd class="r2">クラス編成ですが、壁で仕切らず低いゲートでクラス分けしております。混合保育にて、様々な年齢の幼児との触れ合いを大切にしております。</dd>
        </dl>
        <dl>
            <dt>食事を園でとっていただくことはできますか？</dt>
            <dd>園で昼食、おやつ、夕食をご用意しております</dd>
        </dl>
        <dl>
            <dt>子供が食物アレルギーなのですが、食事やおやつなどの対応を教えてください。</dt>
            <dd>強いアレルギーをお持ちの方は、まずはアレルギー内容をご相談下さい。</dd>
        </dl>
        <dl>
            <dt>持病のため、食事の前に薬を飲ませる必要があるのですが、対応はしていただけますか？</dt>
            <dd class="r2">医療行為を行うことは出来かねますので、必要な場合は掛かりつけ医に事前にご相談ください。</dd>
        </dl>
        <dl>
            <dt>園外に出る事などはありますか？</dt>
            <dd class="r3">散歩や公園に出かけることがございます。楽しく元気いっぱい遊んでいただきます。夏季は、温度によって園内のみの日も設けます。園内には、シャワーも設置しておりますので、汗も洗い流せて、体調維持にも気を付けております。</dd>
        </dl>
        <dl>
            <dt>オムツは布？紙？布団は持ち込みですか？</dt>
            <dd class="r2">足りない分は紙オムツにてご用意しておりますが、定期的に１パックお持ちください。布団は園にてご用意しておりますので、おねしょシーツとタオルケットのみご用意お願いいたします。</dd>
        </dl>
        <dl>
            <dt>トイレトレーニングの指導はありますか？</dt>
            <dd>実施しております。</dd>
        </dl>

    </div>
    <!-- wrapper -->

</section>
<!-- price -->
