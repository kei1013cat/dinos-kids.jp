<script>
    jQuery(function($){
        
        $('#sp_tab #tab1 a').click(function() {
            $('#sp_tab #tab2').removeClass('active');
            $('#sp_tab #tab1').addClass('active');
            $('#sp_tab .photo2').css('display','none');
            $('#sp_tab .photo1').css('display','block');
        });
        $('#sp_tab #tab2 a').click(function() {
            $('#sp_tab #tab1').removeClass('active');
            $('#sp_tab #tab2').addClass('active');
            $('#sp_tab .photo1').css('display','none');
            $('#sp_tab .photo2').css('display','block');
        });
    });
</script>

<section class="program pt_l">
    <section class="dayflow pb_l">
        <ul class="linkbtn cf pt_l pb_l">
            <li class="linkbtn2"><a href="#01">1日の流れ</a></li>
            <li class="linkbtn2"><a href="#02">年間行事</a></li>
        </ul>
        <div class="wrapper" id="01">
            <h2><img class="pc mb mt headline" src="<?php bloginfo('template_url'); ?>/images/head_flow.svg" alt="1日の流れ" ></h2>
            <h2><img class="head sp pt_s pb_l" src="<?php bloginfo('template_url'); ?>/images/head_flow_sp.svg" alt="1日の流れ"></h2>
            <h3 class="pb">基本は下記の流れに沿りますが、<br class="pc">一人ひとりの成長に合わせた対応を行っております。</h3>
            <img class="pc pb_l" src="<?php bloginfo('template_url'); ?>/images/program_dayflow.jpg" alt="ディノスキッズの1日の流れを紹介しております" />

            <div class="sp pb" id="sp_tab">
                <ul class="tab cf">
                    <li id="tab1" class="active"><a href="javascript:void(0);"><img src="<?php bloginfo('template_url'); ?>/images/program_tab1.svg" /></a></li>
                    <li id="tab2" ><a href="javascript:void(0);"><img src="<?php bloginfo('template_url'); ?>/images/program_tab2.svg" /></a></li>
                </ul>
                <div class="photo pt_l">
                    <img class="photo1" src="<?php bloginfo('template_url'); ?>/images/program_event1_sp.jpg" alt="ディノスキッズの年間行事を月別に紹介しております" />
                    <img style="display:none;" class="photo2" src="<?php bloginfo('template_url'); ?>/images/program_event2_sp.jpg" alt="ディノスキッズの年間行事を月別に紹介しております" />
                </div>
                <!-- photo -->
            </div>
            <!-- sp_tab -->

        </div>
        <!-- wrapper -->
    </section>
    <!-- dayflow -->
    
    <section class="event bg_thema1 pt_l" id="02">
        <img class="pc star_left" src="<?php bloginfo('template_url'); ?>/images/program_star_left.png"  />
        <img class="pc star_right" src="<?php bloginfo('template_url'); ?>/images/program_star_right.png" />
        <div class="wrapper">
            <img class="pc event_left" src="<?php bloginfo('template_url'); ?>/images/program_event_left.svg"  />
            <img class="pc event_right" src="<?php bloginfo('template_url'); ?>/images/program_event_right.svg" />
            <h2><img class="pc mb mt headline" src="<?php bloginfo('template_url'); ?>/images/head_event.svg" alt="年間行事" ></h2>
            <h2><img class="head sp pt_s pb_l" src="<?php bloginfo('template_url'); ?>/images/head_event_sp.svg" alt="年間行事"></h2>
            <h3 class="pb">ディノスキッズでは<br class="pc">季節に合わせた様々なイベントを行っております。</h3>
            <img class="pc pb" src="<?php bloginfo('template_url'); ?>/images/program_event.png?v=20190307" alt="ディノスキッズの年間行事を月別に紹介しております"　　　　/>
            <img class="sp pb" src="<?php bloginfo('template_url'); ?>/images/program_event_sp.png?v=20190307" alt="ディノスキッズの年間行事を月別に紹介しております"　　　　/>
            <img class="pc" src="<?php bloginfo('template_url'); ?>/images/program_event_bottom.svg" />
        </div>
        <!-- wrapper -->
        <div class="footer_photo2 mt"></div>
    </section>
    <!-- event -->
</section>
<!-- program -->
