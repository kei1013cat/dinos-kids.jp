<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <?php
  $url = $_SERVER['REQUEST_URI'];
?>
    <meta name="format-detection" content="telephone=no">
    <?php if(is_pc()): ?>
    <meta content="width=1000" name="viewport">
    <?php else: ?>
    <meta name="viewport" content="width=device-width">
    <?php endif; ?>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <title>
        <?php if(is_page() && $post->post_name != 'home'){ wp_title('');echo ' | '; } ?>
        <?php bloginfo('name'); ?>
    </title>

    <!-- fb -->
    <meta property="og:type" content="website" />
    <meta property="og:title" content="ディノスキッズ" />
    <meta property="og:url" content="https://www.dinos-kids.jp/" />
    <meta property="og:image" content="<?php bloginfo('template_url'); ?>/images/ogp2.jpg" />
    <meta property="og:description" content="2019年春・新規開園！ ディノスキッズ保育園はこの春、札幌市内に4ヶ所、保育園を開園します。どの園も地下鉄の駅が近く、お子さまを預けるお父さんお母さんも、園で働く保育士さんにもとても便利な立地です。" />
    <meta property="og:locale" content="ja_JP" />
    <meta property="og:site_name" content="ディノスキッズ" />

    <?php wp_head(); ?>
    <link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/sp.css?v=20190329" media="screen and (max-width: 767px)">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/pc.css?v=20190329" media="screen and (min-width: 768px)">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_url'); ?>/images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_url'); ?>/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_url'); ?>/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="<?php bloginfo('template_url'); ?>/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#2b5797">
    <meta name="theme-color" content="#ffffff">

    <script src="<?php bloginfo('template_url'); ?>/js/jquery-1.11.1.min.js"></script>

    <?php if(($post->post_name =="recruit")):?>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/smoothScroll_rec.js"></script>
    <?php elseif(($post->post_name =="facility")):?>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/smoothScroll_fac.js"></script>
    <?php else: ?>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/smoothScroll.js"></script>
    <?php endif; ?>

    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/izimodal/css/iziModal.min.css" type="text/css">
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/izimodal/js/iziModal.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/izimodal.thema.js"></script>

    <!--<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/pulldown.js"></script>-->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/rollover.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/sp_switch.js"></script>

    <!-- scrollreveal -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/scrollreveal/scrollreveal.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/scrollreveal/scrollreveal.thema.js"></script>


    <script type="text/javascript">
        jQuery(function($) {
            $('#mw_wp_form_mw-wp-form-15 select option[value=""]')
                .html('選択してください');
        });

    </script>

    <?php if(is_pc()):?>
    <!--[if lt IE 9]>
<script src="<?php bloginfo('template_url'); ?>/js/html5.js"></script>
<![endif]-->
    <?php endif; ?>

    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-P3X4T5Q');

    </script>
    <!-- End Google Tag Manager -->

</head>
<?php
    $body_id = "";
    if ( is_home() ) {
        $body_id = ' id="page_index"';
    }else if ($post->post_type == 'jirei') {
        $body_id = ' id="page_'.$post->post_type.'" class="subpage"';
    }else if ( is_page() ) {
        $body_id = ' id="page_'.get_post($post->post_parent)->post_name.'" class="subpage"';
    }else if (  is_post_type_archive('works') || is_singular('works') ) {
        $body_id = ' id="page_works" class="subpage"';
    }else if ( is_single() ) {
        $body_id = ' id="page_single" class="subpage"';
    }else if ( is_archive() ) {
        $body_id = ' id="page_archive" class="subpage"';
    }
?>

<body<?php echo $body_id; ?>>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P3X4T5Q" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div id="outer">
        <header class="global cf">

            <?php
    $parent_id = $post->post_parent; // 親ページのIDを取得
    $parent_slug = get_post($parent_id)->post_name; // 親ページのスラッグを取得
?>

            <div class="pc-menu">
                <h1><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/header_logo.svg" alt="ディノスキッズ" /></a></h1>
                <div class="contact"><a href="<?php bloginfo('url'); ?>/contact/"><img src="<?php bloginfo('template_url'); ?>/images/header_contact.png" alt="園児募集 | 詳細はこちら" /></a></div>
                <nav>
                    <ul class="cf">
                        　　<li><a href="<?php bloginfo('url'); ?>/about/"><img src="<?php bloginfo('template_url'); ?>/images/header_about.svg" alt="当園について" /></a></li>
                        　　<li><a href="<?php bloginfo('url'); ?>/facility/"><img src="<?php bloginfo('template_url'); ?>/images/header_facility.svg" alt="施設紹介" /></a></li>
                        　　<li><a href="<?php bloginfo('url'); ?>/program/"><img src="<?php bloginfo('template_url'); ?>/images/header_program.svg" alt="保育内容" /></a></li>
                        　　<li><a href="<?php bloginfo('url'); ?>/price/"><img src="<?php bloginfo('template_url'); ?>/images/header_price.svg" alt="ご利用料金" /></a></li>
                        　　<li><a href="<?php bloginfo('url'); ?>/question/"><img src="<?php bloginfo('template_url'); ?>/images/header_question.svg" alt="よくあるご質問" /></a></li>
                        　　<li><a href="<?php bloginfo('url'); ?>/recruit/"><img src="<?php bloginfo('template_url'); ?>/images/header_recruit.svg" alt="採用情報" /></a></li>
                    </ul>
                </nav>
            </div>
            <div class="sp-menu">

                <h2><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/header_logo_sp.svg" alt="ディノスキッズ" /></a></h2>

                <div class="menu cf">
                    <p id="closeMenu"> <a href="#"><img src="<?php bloginfo('template_url'); ?>/images/header_close.svg"></a> </p>
                    <p id="openMenu"> <a href="#"> <img src="<?php bloginfo('template_url'); ?>/images/header_menu.svg"> </a> </p>
                </div>
                <!-- menu -->
                <div id="layerMenu">
                    <nav>
                        <ul>
                            <li class="about"><a href="<?php bloginfo('url'); ?>/about/"><img src="<?php bloginfo('template_url'); ?>/images/header_about_sp.svg" alt="当園について" /></a></li>
                            <li class="facility"><a href="<?php bloginfo('url'); ?>/facility/"><img src="<?php bloginfo('template_url'); ?>/images/header_facility_sp.svg" alt="施設紹介" /></a></li>
                            <li class="program"><a href="<?php bloginfo('url'); ?>/program/"><img src="<?php bloginfo('template_url'); ?>/images/header_program_sp.svg" alt="保育内容" /></a></li>
                            <li class="price"><a href="<?php bloginfo('url'); ?>/price/"><img src="<?php bloginfo('template_url'); ?>/images/header_price_sp.svg" alt="ご利用料金" /></a></li>
                            <li class="question"><a href="<?php bloginfo('url'); ?>/question/"><img src="<?php bloginfo('template_url'); ?>/images/header_question_sp.svg" alt="よくあるご質問" /></a></li>
                            <li class="recruit"><a href="<?php bloginfo('url'); ?>/recruit/"><img src="<?php bloginfo('template_url'); ?>/images/header_recruit_sp.svg" alt="採用情報" /></a></li>
                            <li class="contact">
                                <a class="mail" href="<?php bloginfo('url'); ?>/contact/"><img src="<?php bloginfo('template_url'); ?>/images/header_contact_sp.png" alt="お問い合わせ" /></a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <!-- layerMenu -->
                <div id="overray"> </div>

            </div>
        </header>
        <div id="main_contents" class="cf">
