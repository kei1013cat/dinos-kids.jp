<?php
$post = get_page($page_id);
?>
<div id="pagetitle" class="<?php if(is_404()): echo "default"; endif; ?>">
    <div class="wrapper">
        <div class="text">
            <?php if(!is_404()): ?>
                <img class="pc enter-bottom" src="<?php bloginfo('template_url'); ?>/images/pagetitle_<?php echo $post->post_name; ?>_text.svg" alt="<?php echo get_the_title(); ?>">
                <img class="sp" src="<?php bloginfo('template_url'); ?>/images/pagetitle_<?php echo $post->post_name; ?>_text_sp.svg" alt="<?php echo get_the_title(); ?>">
            <?php endif; ?>
        </div>
        <!-- text -->
    </div>
    <!-- wrapper -->
</div>

<!-- page_title -->