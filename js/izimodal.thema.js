$(function() {
	$(document).on('click', '.open-options1', function(event) {
	  event.preventDefault();
	  $('.modal-options1').iziModal('open');
	});
	$('.modal-options1').iziModal({
	  headerColor: '#0a8bcf', //ヘッダー部分の色
	  width: 1000, //横幅
	  overlayColor: 'rgba(51,153,0,0.6)', //モーダルの背景色
	  fullscreen: true, //全画面表示
	  transitionIn: 'fadeInUp', //表示される時のアニメーション
	  transitionOut: 'fadeOutDown' //非表示になる時のアニメーション
	});
});
$(function() {
	$(document).on('click', '.open-options2', function(event) {
	  event.preventDefault();
	  $('.modal-options2').iziModal('open');
	});
	$('.modal-options2').iziModal({
	  headerColor: '#0a8bcf', //ヘッダー部分の色
	  width: 1000, //横幅
	  overlayColor: 'rgba(51,153,0,0.6)', //モーダルの背景色
	  fullscreen: true, //全画面表示
	  transitionIn: 'fadeInUp', //表示される時のアニメーション
	  transitionOut: 'fadeOutDown' //非表示になる時のアニメーション
	});
});

$(function() {
    
    $('#modal-close1').click(function() {
        $('.modal-options1').iziModal('close');
    });
    $('#modal-close2').click(function() {
        $('.modal-options2').iziModal('close');
    });
})    
