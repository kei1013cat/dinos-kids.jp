<section class="price">
    <section class="about pb_l">
    <img class="pc left" src="<?php bloginfo('template_url'); ?>/images/price_about_left.png"  />
    <img class="pc right" src="<?php bloginfo('template_url'); ?>/images/price_about_right.png" />

    <div class="wrapper">
        <h2><img class="pc mb_l mt_l headline" src="<?php bloginfo('template_url'); ?>/images/head_hoikuprice.svg" alt="保育料について"></h2>
        <h2><img class="head pb_l pt_l sp" src="<?php bloginfo('template_url'); ?>/images/head_hoikuprice_sp.svg" alt="保育料について"></h2>
<!--
        <h3 class="headline4 mb">初月保育料および<br class="sp">前月15日以内登園の場合</h3>
        <table class="style01 pb" cellspacing="0" cellpadding="10" >
          <tr class="head">
            <th>プラン</th>
            <td class="th">従業員枠</td>
            <td class="th r2">提携企業枠<span class="small">※お気軽にご相談ください</span></td>
            <td class="th">地域枠</td>
          </tr>
          <tr>
            <th>3ヵ月〜1歳未満</th>
            <td>¥18,000</td>
            <td>¥25,200</td>
            <td>¥36,000</td>
          </tr>
          <tr>
            <th>1歳児</th>
            <td>¥16,000</td>
            <td>¥22,400</td>
            <td>¥32,000</td>
          </tr>
          <tr>
            <th>2歳児</th>
            <td>¥16,000</td>
            <td>¥22,400</td>
            <td>¥32,000</td>
          </tr>
        </table>

        <h3 class="headline4 mt_l mb">前月16日以上登園の場合（2ヵ月目以降適用）</h3>
        <table class="style01 pb_l" cellspacing="0" cellpadding="10">
          <tr class="head">
            <th>プラン</th>
            <td class="th">従業員枠</td>
            <td class="th r2">提携企業枠<span class="small">※お気軽にご相談ください</span></td>
            <td class="th">地域枠</td>
          </tr>
          <tr>
            <th>3ヵ月〜1歳未満</th>
            <td>¥9,000</td>
            <td>¥12,600</td>
            <td>¥18,000</td>
          </tr>
          <tr>
            <th>1歳児</th>
            <td>¥8,000</td>
            <td>¥11,200</td>
            <td>¥16,000</td>
          </tr>
          <tr>
            <th>2歳児</th>
            <td>¥8,000</td>
            <td>¥11,200</td>
            <td>¥16,000</td>
          </tr>
        </table>
-->
        <h3 class="headline4 mb">月額基本料金</h3>
        <table class="style01" cellspacing="0" cellpadding="10">
          <tr class="head">
            <th>プラン</th>
<!--            <td class="th">従業員枠</td>-->
            <td class="th r2">提携企業枠<span class="small">※お気軽にご相談ください</span></td>
            <td class="th">地域枠</td>
          </tr>
          <tr>
            <th>3ヵ月〜1歳未満</th>
<!--            <td>¥9,000</td>-->
            <td>¥12,600</td>
            <td>¥18,000</td>
          </tr>
          <tr>
            <th>1歳児</th>
<!--            <td>¥8,000</td>-->
            <td>¥11,200</td>
            <td>¥16,000</td>
          </tr>
          <tr>
            <th>2歳児</th>
<!--            <td>¥8,000</td>-->
            <td>¥11,200</td>
            <td>¥16,000</td>
          </tr>
        </table>
        <p class="pt_s">・16日以上通園の場合</p>
        <p class="pb_l">・15日以下の通園をご希望の方はお問い合わせください</p>
    </div>
    <!-- wrapper -->
    </section>
    <!-- about -->
    
    <section class="other bg_thema1 pt pb_l">
        <img class="pc left" src="<?php bloginfo('template_url'); ?>/images/price_other_left.png" />
        <img class="pc right" src="<?php bloginfo('template_url'); ?>/images/price_other_right.png" />
        <div class="wrapper">
            <h3 class="headline4 mt_l mb">その他費用</h3>
            <ul>
                <li>おむつ　￥100　　使用数に応じてご請求　</li>
                <li>夕食　　￥350　　前日16時までに申し込み</li>
            </ul>
            <p class="pt_s">※初月ご請求内訳は15日以下登園の場合の当月保育料となります。</p>
            <p>※2ヶ月目以降のご請求内訳は</p>
            <p>◇前月16日以上登園の場合<br>
            16日以上登園の場合の当月保育料＋有償サービス料となります。</p>
            <p>◇前月15日以下登園の場合<br>
            15日以下登園の場合の当月保育料＋有償サービス料となります。</p>
            <p class="pb_s">※上記のほか、園外保育（遠足等）のバス代など必要な実費については、随時お知らせします。</p>
            <p class="pt_s">※表示価格は税別です。（消費税率の改定が行われた場合は改定後の税率により計算）</p>
        </div>
        <!-- wrapper -->
        
    </section>
    <!-- other -->
    
    <section class="paid pb_l mb_l">
        <img class="pc left-img" src="<?php bloginfo('template_url'); ?>/images/price_paid_left.png"  />
        <img class="pc right-img" src="<?php bloginfo('template_url'); ?>/images/price_paid_right.png" />
        <div class="wrapper">
            <h2><img class="pc mb_l mt_l headline" src="<?php bloginfo('template_url'); ?>/images/head_paid.svg" alt="保育料のお支払イメージ"></h2>
            <h2><img class="head pb_l pt_l sp" src="<?php bloginfo('template_url'); ?>/images/head_paid_sp.svg" alt="保育料のお支払イメージ"></h2>
            <h3 class="headline4 mb">入園月</h3>
            <dl class="cf">
                <dt>お支払日<img src="<?php bloginfo('template_url'); ?>/images/right_arrow.svg">入園月の10日</dt>
                <dd>初月保育料</dd>
            </dl>
            <img class="img-center mb_l mt_s" src="<?php bloginfo('template_url'); ?>/images/under_arrow.svg">
            
            <h3 class="headline4 mb">2ヶ月目以降</h3>
            <div class="outer cf pb_l">
                <div class="inner">
                    <h5>お支払日<img src="<?php bloginfo('template_url'); ?>/images/right_arrow.svg">毎月10日</h5>
                    <p>16日以上登園の場合の月額保育料<br>有償サービス料</p>
                </div>
                <!-- inner -->
            </div>
        </div>
        <!-- wrapper -->
    </section>

</section>
<!-- price -->