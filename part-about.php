<section class="top">
    <div class="wrapper">
        <h2><img class="pc mb mt_l headline" src="<?php bloginfo('template_url'); ?>/images/head_business.svg" alt="[企業主導型保育]事業とは" ></h2>
        <h2><img class="head sp pt_l pb_s" src="<?php bloginfo('template_url'); ?>/images/head_business_sp.svg" alt="[企業主導型保育]事業とは"></h2>
    </div>
    <!-- wrapper -->
</section>

<section class="business_top mt mb_l">
    <div class="sp_photo sp">
        <img class="sp about_photo" src="<?php bloginfo('template_url'); ?>/images/about_business_bg_sp.jpg" >
        <img class="sp star_right" src="<?php bloginfo('template_url'); ?>/images/about_star_r.png" />
    </div>
    
    <div class="text">
        <img class="pc star_right" src="<?php bloginfo('template_url'); ?>/images/about_star_r.png" />
        <div class="inner">
        <h3 class="pc pb"><img src="<?php bloginfo('template_url'); ?>/images/business_titlemsg.svg" alt="ディノスキッズは、内閣府の定める企業主導型保育事業則り運営しています。"></h3>
        <h3 class="sp pb_l pt_l mt"><img src="<?php bloginfo('template_url'); ?>/images/business_titlemsg_sp.svg" alt="ディノスキッズは、内閣府の定める企業主導型保育事業則り運営しています。"></h3>
        <p class="pt_s">企業主導型保育所とは、内閣府が始めた企業主導型の事業所内保育事業です。わかりやすく言うと「企業が作った保育園」という意味になります。企業主導型保育事業の目的は、男女問わず仕事と子育ての両立をさせることです。働き方改革による多種多様な就労形態を実現することを目的としています。<br class="pc">
            ディノスキッズは内閣府の定める企業主導型保育事業に則り運営しており、国と札幌市の両方から施設設備のチェックと指導を受けて設立された保育園となります。</p>
        </div>
    </div>
    <!-- text -->
</section>
<!-- business_top -->


<section class="business_feature mt mb_l bg_thema1">
    <div class="wrapper">
        <h2><img class="pc mb mt_l headline" src="<?php bloginfo('template_url'); ?>/images/head_business_feature.svg" alt="[企業主導型保育]事業の特徴" ></h2>
        <h2><img class="head sp pt_l pb_s" src="<?php bloginfo('template_url'); ?>/images/head_business_feature_sp.svg" alt="[企業主導型保育]事業の特徴"></h2>
        <dl class="cf pt    ">
            <dt><img src="<?php bloginfo('template_url'); ?>/images/about_feature1.svg" alt="特徴1"></dt>
            <dd>働き方に応じた多様で柔軟な保育サービスが提供できる。</dd>
        </dl>
        <dl class="cf">
            <dt><img src="<?php bloginfo('template_url'); ?>/images/about_feature2.svg" alt="特徴2"></dt>
            <dd>働きながら子育てしやすいよう環境を整える。</dd>
        </dl>
        <dl class="cf">
            <dt><img src="<?php bloginfo('template_url'); ?>/images/about_feature3.svg" alt="特徴3"></dt>
            <dd>支援の量を拡充させ、待機児童の解消に向け教育・保育の受け皿を増やす。</dd>
        </dl>
        <div class="sp_boxmsg sp">
            このように、内閣府の方針により仕事と子育てを両立させ、
            ワークライフバランスを整えるための整備を行っています。
        </div>
        <div class="boxmsg pb_l pt">
            <img class="star pc" src="<?php bloginfo('template_url'); ?>/images/about_star.png">
            <img class="pc" src="<?php bloginfo('template_url'); ?>/images/about_feature_boxmsg.svg" alt="このように、内閣府の方針により仕事と子育てを両立させ、ワークライフバランスを整えるための整備を行っています。">
            <img class="bgimg" src="<?php bloginfo('template_url'); ?>/images/policy_bg.png">
        </div>
    </div>
    <!-- wrapper -->
</section>
<!-- business_feature -->

<section class="about mt_l mb_l">
    <img class="pc star_left" src="<?php bloginfo('template_url'); ?>/images/about_star_l.png"  />
    <img class="pc star_right" src="<?php bloginfo('template_url'); ?>/images/about_star_r.png" />
    
    <div class="sp_photo sp">
        <img class="sp about_photo" src="<?php bloginfo('template_url'); ?>/images/about_bg_sp.jpg" >
        <img class="sp star_right" src="<?php bloginfo('template_url'); ?>/images/about_star_r.png" />
    </div>
    
    <div class="text">
        <div class="inner">
        <h3 class="pc pb"><img src="<?php bloginfo('template_url'); ?>/images/about_titlemsg.svg" alt="お子様の元気に、お父さま、お母さまのストレスのない元気と笑顔も必要です。"></h3>
        <h3 class="sp pb_l pt_l mt"><img src="<?php bloginfo('template_url'); ?>/images/about_titlemsg_sp.svg" alt="お子様の元気に、お父さま、お母さまのストレスのない元気と笑顔も必要です。"></h3>
        <p class="pt_s">今の仕事は好きだけど、子供を預けたい保育園が近くにない。そのような方に、私たちディノスキッズでは、お父さま、お母さまに社会復帰のお手伝いとして、通勤時の負担を軽減できるよう駅の近くに開設し、ご家族皆さまがストレスない生活を送っていただきたいと想います。<br class="pc">仕事をお探しの方は、保育園の隣に当社の仕事場を設けている園もありますので、当社に勤務いただければ育児と仕事を楽しく両立できる理想の環境です。</p>
        </div>
    </div>
    <!-- text -->
    
</section>
<!-- about -->
<img class="top_photo pc" src="<?php bloginfo('template_url'); ?>/images/about_policy_top_photo.png" >
<img class="top_photo_sp sp" src="<?php bloginfo('template_url'); ?>/images/about_policy_top_photo_sp.png" >

<section class="policy bg_thema1 pb_l">
    <div class="wrapper">
        <h2><img class="pc mb mt headline" src="<?php bloginfo('template_url'); ?>/images/head_policy.svg" alt="私たちのポリシー" ></h2>
        <h2><img class="head sp pt pb_s" src="<?php bloginfo('template_url'); ?>/images/head_policy_sp.svg" alt="私たちのポリシー"></h2>
        <div class="outer cf pb_l">
            <div class="left">
                <h3><img class="pc" src="<?php bloginfo('template_url'); ?>/images/about_policy_text.svg" alt="生きる歓びと感動を共有する" ></h3>
                <h3><img class="sp" src="<?php bloginfo('template_url'); ?>/images/about_policy_text_sp.svg" alt="生きる歓びと感動を共有する" ></h3>
            </div>
            <div class="right">
                <p>昨日よりも今日、今日よりも明日と子どもたちが現在を最もよく生き、未来を作り出す力の基礎が身につくような環境を提供し、子どもたち、保護者、働く一人ひとりがかがやき、生きる歓びと感動を皆で共有したい。その実現のために全力で取り組んでいきます。</p>
            </div>
        </div>
        <!-- outer -->
    </div>
    <!-- wrapper -->
</section>


<section class="aim pb_l">
    <div class="wrapper">
        <img class="bgimg" src="<?php bloginfo('template_url'); ?>/images/policy_bg.png" >
        <h2 class="pc"><img class="mb mt_l pt_l headline" src="<?php bloginfo('template_url'); ?>/images/head_aim.svg" alt="私たちが目指しているところ" ></h2>
        <h2 class="sp pt pb"><img class="head sp pt_l pb_s" src="<?php bloginfo('template_url'); ?>/images/head_aim_sp.svg" alt="私たちが目指しているところ"></h2>
        <h3 class="pb_l">私たちは3つの力をお子様に元気いっぱい楽しみながら、<br class="pc">身に付けて頂きたいと考えております。</h3>
    </div>
    <!-- wrapper -->
    <div class="pc photo mt"></div>
    <img class="sp photo mt" src="<?php bloginfo('template_url'); ?>/images/about_aim_bg_sp.jpg">
    <div class="wrapper">
        <div class="list pb_l">
            <ul class="cf">
                <li>
                    <h4><img class="pc" src="<?php bloginfo('template_url'); ?>/images/about_aim_list1.svg" alt="あいさつのできる子" ></h4>
                    <h4><img class="sp" src="<?php bloginfo('template_url'); ?>/images/about_aim_list1_sp.svg" alt="あいさつのできる子" ></h4>
                    <p class="pt_s">言葉への興味や関心を育て、話したり、聞いたり、相手の話を理解しようとするなど、言葉の豊かさを養うこと。</p>
                </li>
                <li>
                    <h4><img class="pc" src="<?php bloginfo('template_url'); ?>/images/about_aim_list2.svg" alt="自信をもって行動できる子"></h4>
                    <h4><img class="sp" src="<?php bloginfo('template_url'); ?>/images/about_aim_list2_sp.svg" alt="自信をもって行動できる子"></h4>
                    <p class="pt_s">人との関わりの中で、人に対する愛情と信頼感、人権を大切にする心を育てるとともに、自主、自立及び協調の態度を養い、道徳性の芽生えを培うこと。</p>
                </li>
                <li>
                    <h4><img class="pc" src="<?php bloginfo('template_url'); ?>/images/about_aim_list3.svg" alt="いたわりをもてる子"></h4>
                    <h4><img class="sp" src="<?php bloginfo('template_url'); ?>/images/about_aim_list3_sp.svg" alt="いたわりをもてる子"></h4>
                    <p class="pt_s">生命、自然及び社会の事象についての興味や関心を育て、それらに対する豊かな心情や思考力の芽生えを培うこと。</p>
                </li>
            </ul>
        </div>
        <!-- list -->
    </div>
    <!-- wrapper -->
</section>
<!-- aim -->