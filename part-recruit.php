<section class="recruit pb_l mb_l">
    <img class="pc left" src="<?php bloginfo('template_url'); ?>/images/recruit_left.png" />
    <img class="pc right" src="<?php bloginfo('template_url'); ?>/images/recruit_right.png" />
    <div class="wrapper">
        <h2><img class="pc mb_l mt_l headline" src="<?php bloginfo('template_url'); ?>/images/head_boshu.svg" alt="募集要項"></h2>
        <h2><img class="head sp pt_l pb_l" src="<?php bloginfo('template_url'); ?>/images/head_boshu_sp.svg" alt="募集要項"></h2>

        <ul class="linkbtn cf pt_l pb_l">
            <li class="linkbtn2"><a href="#01">園長（正社員）</a></li>
            <li class="linkbtn2"><a href="#02">主任/保育士（正社員）</a></li>
            <li class="linkbtn2"><a href="#03">保育士（正社員）</a></li>
            <li class="linkbtn2"><a href="#04">保育士（契約社員）</a></li>
            <li class="linkbtn2"><a href="#05">調理員（正社員）</a></li>
            <li class="linkbtn2"><a href="#06">調理師（契約社員）</a></li>
            <li class="linkbtn2"><a href="#07">アルバイト</a></li>
            <li class="linkbtn2"><a href="#08">新卒採用について</a></li>
            <li class="linkbtn2"><a href="#09">勤務地</a></li>



        </ul>

    </div>

    <section class="employee pb">
        <div class="wrapper">
            <ul>
                <li id="01">
                    <h3>園長（正社員）</h3>
                    <p>
                        月給 314,000円以上<br>
                        ※経験を考慮<br>
                        ※土日祝勤務が可能な方<br>
                        ※8:30〜20:30のシフト制（週休二日）<br>
                        【年収例】<br>
                        440万円/月給34.1万円＋賞与（主任経験あり・33歳）
                    </p>
                </li>
                <li>
                    <h3 id="02">主任/保育士（正社員）</h3>
                    <p>
                        月給　236,000円～261,000円（初年度）<br>
                        ※保育士経験年数を考慮<br>
                        ※土日祝勤務が可能な方<br>
                        ※8:30〜20:30のシフト制（週休二日）<br>
                        【年収例】<br>
                        361万円/月給26.1万円＋賞与（経験8年・28歳）<br>
                        326万円/月給23.6万円＋賞与（経験１年・33歳）<br>
                        （応募資格）<br>
                        保育士資格をお持ちの方で直近のブランクが少ない方
                    </p>
                </li>
                <li id="03">
                    <h3>保育士（正社員）</h3>
                    <p>
                        月給　216,000円～241,000円（初年度）<br>
                        ※保育士経験年数を考慮<br>
                        ※新規事業のため随時給与見直し/昇格の機会あり<br>
                        ※土日祝勤務が可能な方<br>
                        ※8:30〜20:30のシフト制（週休二日）<br>
                        ※札幌エリアからの転勤なし<br>
                        【年収例】<br>
                        337万円/月給24.1万円＋賞与（経験３年・25歳）<br>
                        302万円/月給21.6万円＋賞与（経験１年・33歳）<br>
                        （応募資格）<br>
                        保育士資格をお持ちの方で直近のブランクが少ない方
                    </p>
                </li>
                <li id="04">
                    <h3>保育士（契約社員）</h3>
                    <p>
                        月給　202,000円～226,000円（初年度）<br>
                        ※保育士経験年数を考慮<br>
                        ※新規事業のため随時正社員登用の機会あり<br>
                        ※8:30〜20:30のシフト制（週休二日）<br>
                        【年収例】<br>
                        316万円/月給22.6万円＋賞与（経験３年・37歳）<br>
                        283万円/月給20.2万円＋賞与（経験１年・21歳）<br>
                        （応募資格）<br>
                        保育士資格をお持ちの方
                    </p>
                </li>
                <li id="05">
                    <h3>調理員（正社員）</h3>
                    <p>
                        月給　184,000円以上（初年度）<br>
                        ※栄養士/管理栄養士の資格がある方、保育園での調理経験のある方歓迎<br>
                        ※土日祝勤務が可能な方<br>
                        ※8:30〜20:30のシフト制（週休二日）<br>
                        【年収例】<br>
                        234万円/月給18.4万円＋賞与（25歳）
                    </p>
                </li>
                <li id="06">
                    <h3>調理師（契約社員）</h3>
                    <p>
                        月給　175,000円以上（初年度）<br>
                        ※栄養士/管理栄養士の資格がある方、保育園での調理経験のある方歓迎<br>
                        ※8:30〜20:30のシフト制（週休二日）<br>
                        【年収例】<br>
                        224万円/月給17.5万円＋賞与（22歳）
                    </p>
                </li>
            </ul>
            <ul>
                <li>
                    <h3>社員待遇</h3>
                    <p>
                        ※昇給年1回・賞与年2回（査定・会社業績により実施）<br>
                        交通費20,000円まで支給／社会保険完備／制服貸与／定期健康診断（年1回）／インフルエンザ予防接種／提携施設・サービス割引制度（ベネフィットステーション）／慶弔見舞金／死亡退職金制度／産前産後休暇／育児／介護休暇／育児・介護制度／子の看護休暇／慶弔休暇／裁判員休暇／他特別休暇（結婚・出産）／年次有給制度 ／（※）退職金制度（確定拠出年金制度）／（※）確定拠出年金マッチング制度／（※）従業員持ち株会<br>
                    </p>
                    <p class="kome">（※）は正社員のみ</p>
                </li>
            </ul>
        </div>
        <!-- wrapper -->
    </section>
    <!-- employee -->

    <section class="pa bg_thema1 pt_l">
        <div class="wrapper">
            <ul>
                <li id="07">
                    <h3>アルバイト</h3>
                    <p>
                        保育士　時給1,200円以上<br>
                        調理員　時給1,100円以上<br>
                        ※詳しくはお問い合わせください
                    </p>
                </li>
                <li id="08">
                    <h3>新卒採用について</h3>
                    <p>
                        2020年3月卒業予定者で、保育士資格取得が見込まれる方、国家試験に向けて現在学習中の方も大歓迎です。（※ただし、ご入社に際しては、保育士資格の取得が必須となります）<br>
                        2019年3月卒業者の方、第二新卒の方も大歓迎！
                    </p>
                </li>
                <li id="09">
                    <h3>勤務地</h3>
                    <p>
                        白石園　　　<br class="sp">札幌市白石区南郷通1丁目8番地ディノス白石<br><br class="sp">
                        麻生園　　　<br class="sp">札幌市北区北40条西4丁目1番1号ASABU LAND<br><br class="sp">
                        月寒中央園　<br class="sp">札幌市豊平区月寒中央通9丁目3-37CBSビル1F<br><br class="sp">
                        東区役所園　<br class="sp">札幌市東区北14条東8丁目3番1号BEビル1F<br>
                    </p>
                </li>
            </ul>
            <p class="linkbtn1 pt"><a href="<?php bloginfo('url'); ?>/contact/">求人のお問い合わせはこちら</a></p>
        </div>
        <!-- wrapper -->
    </section>
    <!-- pa -->



</section>
<!-- price -->
